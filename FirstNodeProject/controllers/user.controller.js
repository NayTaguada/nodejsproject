exports.allAccess = (req, res) => {
    res.status(200).send("Public Content.");
};

exports.userBoard = (req, res) => {
    res.status(200).send("User Content.");
};

exports.adminBoard = (req, res) => {
    res.status(200).send("Admin Content.");
};

exports.moderatorBoard = (req, res) => {
    res.status(200).send("Moderator Content.");
};
const User = require("../models/user.model");
let user_controller = {
    newUser: async (req, res) => {
        try {
            if (req.body == null){throw new Error('request body cannot be null')}
            const user = await new User(req.body);
            await user.generateAuthToken();
            res.status(201).send({data: user});
        } catch (error) {
            res.status(400).send({message: error.message});
        }
    },
    getUser: async (req, res) => {
        try {
            const user = await User.findOne({_id: req.data._id});
            if (!user) {
                res.status(404).send({message: 'Not found'});
            }
            res.status(200).send({ data: user, message: 'User found' });
        } catch (error) {
            res.status(401).send({message: 'Not authorized to access this resource', error: error});
        }
    },
    updateUser:  async (req, res) => {
        try {
            const user = await User.findByIdAndUpdate({_id: req.data._id}, {username: req.body.username, email: req.body.email});
            await user.save();
            res.status(200).send({ data: user, message: 'User has been updated' });
        } catch (error) {
            res.status(400).send({message: error.message});
        }
    },
    deleteUser: async (req, res) => {
      try {
        const deleted = await User.deleteOne({_id: req.data._id});
        if(deleted.deletedCount === 0) {
            res.status(400).send({message: 'User could not be deleted'});
        } else if(deleted.deletedCount > 0) {
            res.status(200).send({ message: 'User has been deleted', data: deleted.deletedCount });
        } else {
            res.status(404).send({message: 'Not found', data: deleted.deletedCount });
        }
      } catch (error) {
        res.status(400).send({message: error.message}); 
      }  
    }
};

module.exports = user_controller;
