let Project = require('../models/project.model');
let fs = require('fs');
let path = require('path');
let project_controller = {
    home: function (req, res) {
        return res.status(200).send({message: 'This is HOME method'});
    },
    test: function (req, res) {
        return res.status(200).send({
            message: 'This is test method'
        });
    },
    saveProject: function (req, res) {
        let project = new Project();
      let requestBody = req.body;
      project.name = requestBody.name;
      project.description = requestBody.description;
      project.category = requestBody.category;
      project.year = requestBody.year;
      project.image = null;

      project.save((e, projectSaved) => {
         if (e) return res.status(500).send({message: 'something went wrong'});
         if (!projectSaved) return res.status(404).send({message: 'Project could not be saved'});

         return res.status(200).send({
              message: 'New project saved',
              project: projectSaved,
          });
      });
  },
  getProject: function (req,res){
      let id = req.params.id;
      if (id == null) return res.status(404).send({message: 'Id not valid'});
      Project.findById(id, (e,project) => {
          if (e) return res.status(500).send({message: 'something went wrong getting the entry'});
          if (!project) return res.status(404).send({message: 'Entry not found'});
          return res.status(200).send({
              message: 'Data obtained:',
              project: project,
          });
      });
  },
  getProjects: function (req, res){
      Project.find({}).sort('-year').exec((e,projects) => {
         if (e) return res.status(500).send({ message: 'Error getting all projects'   });
         if (!projects) return res.status(404).send({message: 'Not projects found'});
         return res.status(200).send({projects});
      });
  },
  updateProject: function(req,res){
      let id = req.params.id;
      let update = req.body;
      if (id == null){
          return res.status(404).send({message: 'Id not valid'});
      }
      Project.findByIdAndUpdate(id, update, (e,projectUpdated) => {
          if (e) return res.status(500).send({message: 'Error updating the project'});
          if (!projectUpdated) return res.status(404).send({message: 'Project not found'});
          return res.status(200).send({ project: projectUpdated});
      });
  },
  deleteProject:  function (req,res){
      let id = req.params.id;
      Project.findByIdAndDelete(id,({}),(e,projectDeleted) => {
          if (e) return res.status(500).send({message: 'Error updating the project'});
          if (!projectDeleted) return res.status(404).send({message: 'Project not found'});
          return res.status(200).send({ message: 'entity deleted'});
      });
  },
  uploadImage: function (req,res){
      let id = req.params.id;
      let fileName = 'No image';
      let pImage = req.files;

      if(pImage){
          let filePath = pImage.image.path.split('\\');
          fileName = filePath[1];
          let fileExt = fileName.split('\.');
          if (fileExt[1] === 'jpg' || fileExt[1] === 'jpeg' || fileExt[1] === 'png'){
              Project.findByIdAndUpdate(id,{image: fileName},{new: true},(e, projectUploaded) => {
                  if (e) return res.status(500).send({ message: 'Error updating the project'});
                  if (!projectUploaded) return res.status(404).send({message: 'Project not found'});
                  return res.status(200).send({project: projectUploaded});
              });
          } else {
              fs.unlink(pImage.image.path, (e) => {
                  return res.status(400).send({error: 'File format not supported', errorDetail: e});
              });
          }
      } else {
          return res.status(200).send({ message: fileName });
      }
  },
  getImageFile: function (req,res) {
    let file = req.params.image;
    let imagePath = './uploads/' + file;

    fs.access(imagePath, fs.constants.F_OK, (err) => {
        if(err){
            return res.status(200).send({message: "File doesn't exist.."})
        } else {
            return res.sendFile(path.resolve(imagePath));
        }
    })

  }

};

module.exports = project_controller;
