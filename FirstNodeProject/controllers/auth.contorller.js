let User = require("../models/user.model");
let auth_controller = {
    login: async (req, res) => {
        //Login a registered user
        try {
            let {email, password} = req.body;
            if (!email || !password) {
                res.status(400).send({message: 'Missing mandatory'});
            }
            let user = await User.findByCredentials(email, password);
            if (!user) {
                res.status(401).send({message: 'Login failed! Check authentication credentials'});
            }
            await user.generateAuthToken();
            res.status(200).send({user});
        } catch (error) {
            res.status(400).send({message: error.message});
        }
    },
    logout: async (req, res) => {
        try {
            const user = await User.findOne({_id: req.data._id});
            user.tokens = user.tokens.filter((token) => {
                return token.token !== req.token;
            });
            await user.save();
            res.status(200).send({message: 'Session closed'});
        } catch (error) {
            res.status(400).send({message: error.message});
        }
    },
    logoutAll: async (req, res) => {
        try {
            const user = await User.findOne({_id: req.data._id});
            user.tokens.splice(0, user.tokens.length)
            await user.save()
            res.status(200).send({message: 'All sessions were removed'});
        } catch (error) {
            res.status(400).send({message: error.message});
        }
    }
};

module.exports = auth_controller;
