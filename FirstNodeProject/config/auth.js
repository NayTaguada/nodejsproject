module.exports = {
    secret: "secret-key",
    privateKey: "-----BEGIN RSA PRIVATE KEY-----\n" +
        "MIIEoQIBAAKCAQB0jI5z2Xta394eB5ZANnvy0vsGWI+neMEPpzfd+8gImh6UpbfU\n" +
        "mY/VyAVQ9mIo37Z5A4m/FlLfqz7LGYwQnA8FVofNVruhuXWcLqv/fkGS3XbR+P6B\n" +
        "so/aS4sBX43r7httX3vi9V4BZb6ropzBVUev/dt23T5OldDgziJ6CzkWtCfquHdP\n" +
        "dRNmwUIiaVATskZIgpIj2FAYzJQgH6uv1CS9XGmxJaFIspZtiQz4/0eQ1nqK+Vip\n" +
        "/srHtexhH7vN9tTnr2zWoUM7NyAKCnOF2JzbljMJ14U+FasN+yg3+md/FL4cVuOw\n" +
        "lZfY4NwUiL31Kw2Z9rKZofImzOzWXSw2+GvdAgMBAAECggEAJwiQCRK0con5fPsz\n" +
        "ggv9VST993Efo4oYtqJdXpYdt2vOhCBAQg6wEnwr86Kg22mZVkwuDPZwCQL0Q9cp\n" +
        "hsKNcU86qjW7H33aetVSu33Qflx3l/kZW9yuuwCD7JDRGkwjmWzaM/fTZQprYlZS\n" +
        "w5lJKhMONnAAlxcr3q6K827gzs5RS8RvvFA53h7nl6PnsaHR2sBFd9jtRgnjAm4z\n" +
        "aFTe1mrbsw3gYdFhuo8RVRqomGlyrksoWYcJuHcCn7oMlxdlCGOkOKwFhcFqp+2g\n" +
        "ScZ3jKno1/fW4H4qUgTAid/q9x2o1hZz9tprSvaeow7jeqEHZiT09L/mSsEYxotM\n" +
        "4eaJTQKBgQC8w2gYlYiTTz6MfEUpLxV4KI2uEq2G4Zy5LBTfpoFYvd5ZI0fqTO7+\n" +
        "q1agtNvw01fn9gmDVfJuf683zPwtIZTjS5eoXBMTr4gG3f19eY3hPTzclmrSrOVi\n" +
        "1PKcTE8lxsk9ef9T44xfjMI/uVBUDH57yLIS/cp2/lcfMnk/N2qyAwKBgQCeEDY8\n" +
        "9rDLFV3/HEMpOCy/y6buHplInYUOTbU8dySMrI669R2OHlNQhOt5yDPtfSWdX5oc\n" +
        "iXxHkqzTgFyQd8fkYs5W2cvBKnJoqnGRLaD34b8VUukl2CXzVZudlVTGQyzo8WGs\n" +
        "XGvhAQUbia5k+B9HYS26H4L0t9J5eDi/rAP0nwKBgC0rycZQSl2OEhbGSjqfxNB7\n" +
        "S+RDHflwFfQOA3S9wKg+z22fTr9X571TvSg5/4Py309oBkDCmYAt1/1PdW1LYvi9\n" +
        "hy5H0lzcnX2fM+EJ7JTg9aaH7Eo8C6yJ7wDOFgahOlkWz64AfwACjB15uCpUzxPK\n" +
        "JCr9Rq0REkIp7u/c0BCHAoGARcf4ef+a232KaucKS3dOYjpBbxy5JVLNfBwYaCJd\n" +
        "U3GW7bjrHGQM+h5EaxZUvoMiiRC8095qrJp3jEQ+GVWYPR7V44fxqUzZB5Y/3w0a\n" +
        "5HSYTIy2+4sqbfwcYHyd2Y2slQaDzXlb8xDlNVwaTt8vx+6CJopk4S8KT7AgtZX2\n" +
        "hRcCgYBg2kJY65scjCFvmjChiOpLlD3I3nbUgri3XlKZ7tTTuOsjxHE5GRLeUwUj\n" +
        "wu5HOSX9w66FFe1b4Ao98u9GxuNqKeGcSvJ2OK/ZffTpMfggpYjZHi3Wk7dmF5xW\n" +
        "OH5JKFpt6MaZJKm7nqfDQlwPhOCko0e1pVxY9wcrrJWkAo/oeA==\n" +
        "-----END RSA PRIVATE KEY-----",
    publicKey: "-----BEGIN PUBLIC KEY-----\n" +
        "MIIBITANBgkqhkiG9w0BAQEFAAOCAQ4AMIIBCQKCAQB0jI5z2Xta394eB5ZANnvy\n" +
        "0vsGWI+neMEPpzfd+8gImh6UpbfUmY/VyAVQ9mIo37Z5A4m/FlLfqz7LGYwQnA8F\n" +
        "VofNVruhuXWcLqv/fkGS3XbR+P6Bso/aS4sBX43r7httX3vi9V4BZb6ropzBVUev\n" +
        "/dt23T5OldDgziJ6CzkWtCfquHdPdRNmwUIiaVATskZIgpIj2FAYzJQgH6uv1CS9\n" +
        "XGmxJaFIspZtiQz4/0eQ1nqK+Vip/srHtexhH7vN9tTnr2zWoUM7NyAKCnOF2Jzb\n" +
        "ljMJ14U+FasN+yg3+md/FL4cVuOwlZfY4NwUiL31Kw2Z9rKZofImzOzWXSw2+Gvd\n" +
        "AgMBAAE=\n" +
        "-----END PUBLIC KEY-----"
};
