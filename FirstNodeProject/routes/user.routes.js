let auth = require("../middlewares/authJwt");
let controller = require("../controllers/user.controller");
let express = require('express');
let UserRoutes = express.Router();

UserRoutes.post('/api/users', controller.newUser);
UserRoutes.get('/api/users/:userId', auth, controller.getUser);
UserRoutes.put('/api/users/:userId', auth, controller.updateUser);
UserRoutes.delete('/api/users/:userId', auth, controller.deleteUser);

module.exports = UserRoutes;