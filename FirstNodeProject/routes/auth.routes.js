let AuthController = require("../controllers/auth.contorller");
let auth = require("../middlewares/authJwt");
let express = require('express');
let AuthRouter = express.Router();

AuthRouter.post('/api/users/login', AuthController.login);
AuthRouter.get('/api/users/logout', auth, AuthController.logout);
AuthRouter.get('/api/users/logout-all', auth, AuthController.logoutAll);

module.exports = AuthRouter;
