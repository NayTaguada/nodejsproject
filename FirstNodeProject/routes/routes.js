const projectRoutes = require('./project.routes');
const authRoutes = require('./auth.routes');
const userRoutes = require('./user.routes');

module.exports = { projectRoutes, authRoutes, userRoutes };