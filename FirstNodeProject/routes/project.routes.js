let express = require('express');
let ProjectController = require('../controllers/project.controller');
let ProjectRouter = express.Router();

let multiparty = require('connect-multiparty');
let multipartyMiddleWare = multiparty({uploadDir: './uploads'})

ProjectRouter.get('/home', ProjectController.home);
ProjectRouter.post('/test', ProjectController.test);
ProjectRouter.post('/save-project', ProjectController.saveProject);
ProjectRouter.get('/project/:id', ProjectController.getProject);
ProjectRouter.get('/projects', ProjectController.getProjects);
ProjectRouter.put('/update-project/:id', ProjectController.updateProject);
ProjectRouter.delete('/delete-project/:id', ProjectController.deleteProject);
ProjectRouter.put('/upload-image/:id', multipartyMiddleWare, ProjectController.uploadImage);
ProjectRouter.get('/get-image/:image', ProjectController.getImageFile);

module.exports = ProjectRouter;
