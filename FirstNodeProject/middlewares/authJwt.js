let {verify} = require("jsonwebtoken");
let config = require('../config/auth');

let auth = async (req, res, next) => {
    const token = req.header('Authorization').replace('Bearer ', '');
    let data;
    verify(token, config.publicKey, {algorithm: 'RS256'}, (err, decoded) => {
        if (err) return res.status(400).send({message: 'Incorrect token', error: err});
        data = decoded;
    });
    req.data = data;
    req.token = token;
    next();
};

module.exports = auth;