'use strict'
let validator = require('validator');
let mongoose = require('mongoose');
let {sign} = require('jsonwebtoken');
let bcrypt = require('bcryptjs');
let config = require('../config/auth');
let Schema = mongoose.Schema;
let UserSchema = Schema({
    username: {
        type: String,
        required: true,
        trim: true
    },
    email: {
        type: String,
        required: true,
        unique: true,
        lowercase: true,
        validate: (value) => {
            if (!validator.isEmail(value)) {
                throw new Error('Invalid Email address');
            }
        }
    },
    password: {
        type: String,
        required: true,
        validator: /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/,
    },
    roles: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: "Role",
    }],
    tokens: [{
        token: {
            type: String,
            required: false
        }
    }]
}, {timestamps: {createdAt: 'created_at', lastModified: 'modified_at'}});

UserSchema.pre('save', async function (next) {
    // Hash the password before saving the user model
    const user = this;
    if (user.isModified('password')) {
        user.password = await bcrypt.hash(user.password, 10);
    }
    next();
});

UserSchema.methods.generateAuthToken = async function () {
    // Generate an auth token for the user
    const user = this;
    const token = sign({_id: this._id, email: this.email}, config.privateKey, {algorithm: 'RS256'});
    user.tokens = user.tokens.concat({token});
    await user.save();
    return token;
}

UserSchema.statics.findByCredentials = async (email, password) => {
    // Search for a user by email and password.
    const user = await User.findOne({email});
    if (!user) {
        throw new Error('Invalid login credentials');
    }
    const isPasswordMatch = await bcrypt.compare(password, user.password);
    if (!isPasswordMatch) {
        throw new Error('Invalid login credentials');
    }
    return user;
}

const User = mongoose.model("User", UserSchema);
module.exports = User;
