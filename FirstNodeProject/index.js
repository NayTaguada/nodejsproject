const express = require("express");
const createServer = require('./server');
let mongoose = require('mongoose');
mongoose.Promise = global.Promise;
const port = 3700;
const db = {};
db.user = require("./models/user.model");
db.role = require("./models/role.model");
db.ROLES = ["user", "admin", "moderator"];

let Role = db.role;

mongoose.connect('mongodb://localhost:27017/', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true
})
    .then(() => {
        const app = createServer();
        app.listen(port, () => {
            console.log(`Server is running on ${port}`);
        });
    })
    .catch((e) => {
        console.log(e)
    })