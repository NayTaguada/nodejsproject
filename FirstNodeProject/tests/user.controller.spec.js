//const mongoose = require('mongoose');
jest.mock('../models/user.model');
const model = require('../models/user.model');
const controller = require('../controllers/user.controller');
const { mockRequest, mockResponse } = require('./util/interceptor');
const mockUser = { username: "Random", email: "randomtron@gmail.com",  password: "123456aA!", save: () => "Good" };

describe('User controller methods', () => {
    test('newUser: should retrieve 201 and return correct value', async () => {
        let req = await mockRequest(mockUser);
        req.data._id = 1;
        const res = mockResponse();
        await controller.newUser(req, res);
        expect(res.send.mock.calls.length).toBe(1);
        expect(res.status).toHaveBeenCalledWith(201);
    });
    test('newUser: should retrieve an error if request body is null', async () => {
        const req = mockRequest(null);
        const res = mockResponse();
        await controller.newUser(req, res);
        expect(res.send.mock.calls.length).toBe(1);
        expect(res.status).toHaveBeenCalledWith(400);
    });
    test('updateUser: should retrieve 200 and return correct value', async () => {
        jest.spyOn(model, 'findByIdAndUpdate').mockImplementationOnce(() => mockUser);
        let req = mockRequest(mockUser);
        const res = mockResponse();
        await controller.updateUser(req, res);
        expect(res.send.mock.calls.length).toBe(1);
        expect(res.status).toHaveBeenCalledWith(200);
    });
    test('updateUser: should retrieve an error if req body is null', async () => {
        const req = mockRequest(null);
        const res = mockResponse();
        await controller.updateUser(req, res);
        expect(res.send.mock.calls.length).toBe(1);
        expect(res.status).toHaveBeenCalledWith(400);
    });
    test('getUser: should retrieve 200 and return correct value', async () => {
        jest.spyOn(model, 'findOne').mockImplementationOnce(() => mockUser);
        let req = mockRequest(mockUser);
        const res = mockResponse();
        await controller.getUser(req, res);
        expect(res.send.mock.calls.length).toBe(1);
        expect(res.status).toHaveBeenCalledWith(200);
    });
    test('deleteUser: should retrieve 200 and return correct value', async () => {
        jest.spyOn(model, 'deleteOne').mockImplementationOnce(() => res.status(200));
        let req = mockRequest(mockUser);
        const res = mockResponse();
        await controller.deleteUser(req, res);
        expect(res.send.mock.calls.length).toBe(1);
        expect(res.status).toHaveBeenCalledWith(200);
    });
    test('deleteUser: should retrieve an error if the id is null', async () => {
        const req = mockRequest(null);
        const res = mockResponse();
        await controller.deleteUser(req, res);
        expect(res.send.mock.calls.length).toBe(1);
        expect(res.status).toHaveBeenCalledWith(400);
    });

});