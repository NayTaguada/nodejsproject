jest.mock('../models/user.model');
const model = require('../models/user.model');
const controller = require('../controllers/auth.contorller');
const { mockRequest, mockResponse } = require('./util/interceptor');

describe('Auth controller methods', () => {
    test('login with user should work', async () => {
        const mockUser = { username: "Random", email: "randomtron@gmail.com",  password: "123456aA!", generateAuthToken: () => "Good" };
        jest.spyOn(model, 'findByCredentials').mockImplementationOnce(() => mockUser);
        let req = mockRequest(mockUser);
        req.data._id = 1;
        const res = mockResponse();
        await controller.login(req, res);
        expect(res.send.mock.calls.length).toBe(1);
        expect(res.status).toHaveBeenCalledWith(200);
    });
    test('login with null req body should throw an error', async () => {
        jest.spyOn(model, 'findByCredentials').mockImplementationOnce(() => mockUser);
        let req = mockRequest(null);
        req.data._id = 1;
        const res = mockResponse();
        await controller.login(req, res);
        expect(res.send.mock.calls.length).toBe(1);
        expect(res.status).toHaveBeenCalledWith(400);
    });
    test('logout with user should work', async () => {
        const mockUser = { tokens: Array(2), save: () => "Good"};
        jest.spyOn(model, 'findOne').mockImplementationOnce(() => mockUser);
        let req = mockRequest(mockUser);
        req.data._id = 1;
        const res = mockResponse();
        await controller.logout(req, res);
        expect(res.send.mock.calls.length).toBe(1);
        expect(res.status).toHaveBeenCalledWith(200);
    });
    test('logout without sessions should throw an error', async () => {
        const mockUser = { save: () => "Good"};
        jest.spyOn(model, 'findOne').mockImplementationOnce(() => mockUser);
        let req = mockRequest(mockUser);
        req.data._id = 1;
        const res = mockResponse();
        await controller.logout(req, res);
        expect(res.send.mock.calls.length).toBe(1);
        expect(res.status).toHaveBeenCalledWith(400);
    });
    test('logout all with user should work', async () => {
        const mockUser = { tokens: Array(2), save: () => "Good"};
        jest.spyOn(model, 'findOne').mockImplementationOnce(() => mockUser);
        let req = mockRequest(mockUser);
        req.data._id = 1;
        const res = mockResponse();
        await controller.logout(req, res);
        expect(res.send.mock.calls.length).toBe(1);
        expect(res.status).toHaveBeenCalledWith(200);
    });
    test('logout all without sessions should throw an error', async () => {
        const mockUser = { save: () => "Good"};
        jest.spyOn(model, 'findOne').mockImplementationOnce(() => mockUser);
        let req = mockRequest(mockUser);
        req.data._id = 1;
        const res = mockResponse();
        await controller.logout(req, res);
        expect(res.send.mock.calls.length).toBe(1);
        expect(res.status).toHaveBeenCalledWith(400);
    });

});