const mongoose = require('mongoose');

module.exports = {
    mockRequest: (bodyData) => {
        const req = {}
        req.body = bodyData;
        req.data = jest.fn().mockReturnValue(req);
        req.data._id = mongoose.Types.ObjectId();
        return req;
    },
    mockResponse: () => {
        const res = {}
        res.send = jest.fn().mockReturnValue(res);
        res.status = jest.fn().mockReturnValue(res);
        res.json = jest.fn().mockReturnValue(res);
        return res;
    }
}