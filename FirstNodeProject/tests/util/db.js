const mongoose = require('mongoose');

const db = {
    connectDB: (done) => {
        mongoose.connect(
            "mongodb://localhost:27017/testing",
            {
                useNewUrlParser: true,
                useUnifiedTopology: true,
                useCreateIndex: true 
            },() => done()
        )
    },
    dropDB: (done) => {
        mongoose.connection.db.dropDatabase(() => {
            mongoose.connection.close(() => done())
        })
    },
    insertOne: async (e) => {
      return await mongoose.Collection.insertOne(e);
    }   
}

module.exports = db;