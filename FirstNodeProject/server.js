const express = require('express');
let routes = require('./routes/routes');

function createServer() {
    let cors = require("cors");
    let corsOptions = {
        origin: "http://localhost:3700"
    };
    const app = express();
    app.use(cors(corsOptions));
    app.use(express.json());
    app.use(express.urlencoded({extended: true}));
    app.use(routes.authRoutes);
    app.use(routes.userRoutes);
    app.use(routes.projectRoutes);
    return app;
}

module.exports = createServer;